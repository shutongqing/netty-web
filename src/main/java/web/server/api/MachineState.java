package web.server.api;

import web.server.annotation.Func;
import web.server.annotation.RequestMethod;
import web.server.ioc.Api;
import web.server.dto.License;
import web.server.dto.License2;

@Func(path = "/MachineState")
public class MachineState {


    @Func(path = "/health", method = RequestMethod.GET)
    public String health() {

        return "health-11900";
    }

    @Func(method = {RequestMethod.POST, RequestMethod.GET})
    public String cpu() {

        return "cpu-11900";
    }

    @Func(method = {RequestMethod.POST, RequestMethod.GET})
    public Api api(){
        return new Api("mm", null, new MachineState());
    }

    @Func(method = {RequestMethod.POST, RequestMethod.GET})
    public int getInt(License license){
        System.out.println(license);
        return 11;
    }

    @Func(method = {RequestMethod.POST, RequestMethod.GET})
    public float getFloat(License2 license2){
        System.out.println(license2);
        return 11.1F;
    }
}
