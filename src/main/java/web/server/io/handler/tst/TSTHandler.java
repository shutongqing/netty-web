package web.server.io.handler.tst;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import web.server.utils.ByteHelper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TSTHandler extends SimpleChannelInboundHandler<byte[]> {


    ExecutorService executorService = Executors.newCachedThreadPool();

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, byte[] bytes) throws Exception {

        executorService.submit(() -> {
            String string = ByteHelper.bytesToHexString(bytes);
            System.out.println("-----------------------------");
            System.out.println(string.length());
            System.out.println(string);
        });

    }
}

