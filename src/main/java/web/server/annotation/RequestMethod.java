package web.server.annotation;

public enum RequestMethod {
    GET,
    POST,;

    private RequestMethod() {
    }
}
