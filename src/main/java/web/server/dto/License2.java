package web.server.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class License2 {
    private String name;

    private long time;
}
