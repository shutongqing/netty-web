package web.server;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ServerConfig {

    List<String> apis;

    ServerConfig.Server server;

    @Getter
    @Setter
    @ToString
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Server {
        Integer port;
        String name;
    }
}
