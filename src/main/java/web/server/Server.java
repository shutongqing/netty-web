package web.server;

import org.yaml.snakeyaml.Yaml;
import web.server.io.handler.http.HttpServer;
import web.server.ioc.ApiStore;

import java.io.InputStream;

public class Server {

    public static void main(String[] args) throws Exception {
        start();
    }

    public static void start() throws Exception {
        ServerConfig serverConfig = defaultConfig();
        ApiStore.register(serverConfig);
        HttpServer httpServer = new HttpServer(serverConfig.server.port);
        httpServer.start();
    }


    private static ServerConfig defaultConfig() {

        InputStream inputStream = Server.class.getClassLoader().getResourceAsStream("application.yaml");
        // 使用 SnakeYAML 解析配置文件
        Yaml yaml = new Yaml();
        return yaml.loadAs(inputStream, ServerConfig.class);
    }
}
