package web.server.ioc;

import lombok.*;
import web.server.utils.JsonHelper;

import java.lang.reflect.Method;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Api {

    String methodName;

    Method method;

    Object cls;

    public Object invoke(Object[] args) throws Exception {

        Object result = method.invoke(cls, args);
        if (result == null) {
            return null;
        }
        return JsonHelper.beanToJson(result);
    }
}
